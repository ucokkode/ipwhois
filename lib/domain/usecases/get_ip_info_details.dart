import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:ipwhois/common/failures.dart';
import 'package:ipwhois/common/usecase.dart';
import 'package:ipwhois/domain/entities/ipinfo.dart';
import 'package:ipwhois/domain/repositories/ipinfo_repository.dart';

class GetIpInfoDetailsUseCase implements UserCases<IpInfo, Params> {
  final IpInfoRepository repository;

  const GetIpInfoDetailsUseCase({required this.repository});

  @override
  Future<Either<Failure, IpInfo>> call(Params param) async {
    if (param is Hostname) {
      return await repository.getInfoByHostname(param.hostname);
    } else {
      return await repository.getInfoDetails();
    }
  }
}

class Hostname extends Params {
  final String hostname;

  const Hostname({required this.hostname});

  @override
  List<Object> get props => [hostname];
}

class NoParams extends Params {}

class Params extends Equatable {
  const Params();

  @override
  List<Object> get props => [];
}
