import 'package:flutter_test/flutter_test.dart';
import 'package:ipwhois/data/mappers/ipwhoisdto_to_ipinfo.dart';
import 'package:ipwhois/domain/entities/ipinfo.dart';

import '../../data_for_test/import_data.dart';

void main() {
  final toJson = mockInfo.toIpInfo();
  test("should equal", () {
    const actual = IpInfo(
      ip: "194.63.249.234",
      country: "Norway",
      flag: "https://cdn.ipwhois.io/flags/no.svg",
    );
    expect(toJson == actual, true);
    expect(
        actual ==
            const IpInfo(
                ip: "194.63.249.234",
                country: "Norway",
                flag: "https://cdn.ipwhois.io/flags/no.svg"),
        true);
  });
  test("should not equal", () {
    const actual = IpInfo(
      ip: "194.63.249.234",
      country: "Finland",
      flag: "https://cdn.ipwhois.io/flags/no.svg",
    );
    expect(actual == toJson, false);
    expect(
        actual ==
            const IpInfo(
                ip: "194.63.249.234",
                country: "Norway",
                flag: "https://cdn.ipwhois.io/flags/no.svg"),
        false);
  });
}
