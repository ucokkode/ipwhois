import 'package:flutter_test/flutter_test.dart';
import 'package:ipwhois/data/remote/ipwhois.dto.dart';

import '../../data_for_test/import_data.dart';

void main() {
  group("IpWhoisUnion", () {
    group("fromJson", () {
      test("should return IpWhoisDTO from success mocking json", () {
        final Map<String, dynamic> data = includeJson("info.json");
        final union = IpWhoisUnion.fromJson(data);
        expect(union, isA<IpWhoisDTO>());
        expect(union, mockInfo);
      });
      test("should return IpWhoisError from fail mocking json", () {
        final Map<String, dynamic> data = includeJson("fail.json");
        final union = IpWhoisUnion.fromJson(data);
        expect(union, isA<IpWhoisError>());
        expect(union, mockInfoFail);
      });
    });
    group("toJson", () {
      test("should return Map from IpWhoisDTO mock", () {
        final data = mockInfo.toJson();
        final map = {
          "ip": "194.63.249.234",
          "success": true,
          "country": "Norway",
          "country_flag": "https://cdn.ipwhois.io/flags/no.svg",
        };
        expect(data, map);
      });
      test("should return Map from IPWhoisError mock", () {
        final data = mockInfoFail.toJson();
        final map = {
          "success": false,
          "message": "invalid IP address",
        };
        expect(data, map);
      });
    });
  });
}
