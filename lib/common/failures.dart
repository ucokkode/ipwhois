import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  @override
  List<Object> get props => [];
}

class ServerFailure extends Failure {}

class InvalidHostname extends Failure {}

class UnexpectedFailure extends Failure {}
