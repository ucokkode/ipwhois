part of 'ip_info_bloc.dart';

abstract class IpInfoEvent extends Equatable {
  const IpInfoEvent();

  @override
  List<Object> get props => [];
}

class GetInfoByHostname extends IpInfoEvent {
  final String hostname;

  const GetInfoByHostname(this.hostname);

  @override
  List<Object> get props => [hostname];
}

class GetInfo extends IpInfoEvent {}
