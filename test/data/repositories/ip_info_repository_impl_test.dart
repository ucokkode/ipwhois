import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:ipwhois/common/failures.dart';
import 'package:ipwhois/data/remote/ipwhois.resource.dart';
import 'package:ipwhois/data/repositories/ipinfo_repository_impl.dart';
import 'package:ipwhois/domain/entities/ipinfo.dart';
import 'package:ipwhois/domain/repositories/ipinfo_repository.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../data_for_test/import_data.dart';
import 'ip_info_repository_impl_test.mocks.dart';

@GenerateMocks([IpWhoisDataResource])
void main() {
  late IpWhoisDataResource resource;
  late IpInfoRepository repository;
  setUp(() {
    resource = MockIpWhoisDataResource();
    repository = IpInfoRepositoryImpl(resource: resource);
  });
  group("getInfoDetails", () {
    test("should return future of IPInfo", () async {
      when(resource.getInfoDetails())
          .thenAnswer((_) async => Future.value(mockInfo));

      final result = await repository.getInfoDetails();
      expect(result.isRight(), true);
      result.fold(
        (_) {
          fail("shouldn't return an Failure");
        },
        (info) {
          expect(info, isA<IpInfo>());
          expect(info.ip, "194.63.249.234");
        },
      );
    });
    test("should return ServerFailure", () async {
      when(resource.getInfoDetails())
          .thenThrow(const HttpException("failed to load"));
      final result = await repository.getInfoDetails();
      result.fold(
        (failure) {
          expect(failure, isA<Failure>());
          expect(failure, ServerFailure());
        },
        (_) {
          fail("shouldn't return an IpInfo");
        },
      );
    });
    test("should return Unexpected failure", () async {
      when(resource.getInfoDetails()).thenThrow(Exception());
      final result = await repository.getInfoDetails();
      result.fold(
        (failure) {
          expect(failure, isA<Failure>());
          expect(failure, UnexpectedFailure());
        },
        (_) {
          fail("shouldn't return an IpInfo");
        },
      );
    });
  });

  group("getInfoByHostname", () {
    const hostname = "google.com";
    test("should return future of IPInfo", () async {
      when(resource.getInfoByHostname(hostname))
          .thenAnswer((_) async => Future.value(mockInfoByHostname));

      final result = await repository.getInfoByHostname(hostname);
      expect(result.isRight(), true);
      result.fold(
        (_) {
          fail("shouldn't return an Failure");
        },
        (info) {
          expect(info, isA<IpInfo>());
          expect(info.ip, "74.125.24.102");
        },
      );
    });
    test("should return future of IpWhoisError", () async {
      when(resource.getInfoByHostname("hostname.fail"))
          .thenAnswer((_) => Future.value(mockInfoFail));
      final result = await repository.getInfoByHostname("hostname.fail");
      expect(result.isLeft(), true);
      result.fold(
        (failure) {
          expect(failure, isA<Failure>());
          expect(failure, InvalidHostname());
        },
        (_) {
          fail("shouldn't return an IpInfo");
        },
      );
    });
    test("should return ServerFailure", () async {
      when(resource.getInfoByHostname(hostname))
          .thenThrow(const HttpException("failed to load"));
      final result = await repository.getInfoByHostname(hostname);
      result.fold(
        (failure) {
          expect(failure, isA<Failure>());
          expect(failure, ServerFailure());
        },
        (_) {
          fail("shouldn't return an IpInfo");
        },
      );
    });
    test("should return Unexpected failure", () async {
      when(resource.getInfoByHostname(hostname)).thenThrow(Exception());
      final result = await repository.getInfoByHostname(hostname);
      result.fold(
        (failure) {
          expect(failure, isA<Failure>());
          expect(failure, UnexpectedFailure());
        },
        (_) {
          fail("shouldn't return an IpInfo");
        },
      );
    });
  });
}
