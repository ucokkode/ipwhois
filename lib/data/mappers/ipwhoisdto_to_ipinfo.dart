import 'package:ipwhois/data/remote/ipwhois.dto.dart';
import 'package:ipwhois/domain/entities/ipinfo.dart';

extension IPWhoisDTOtoIpInfo on IpWhoisDTO {
  IpInfo toIpInfo() {
    return IpInfo(ip: ip, country: country, flag: flag);
  }
}
