import 'package:dartz/dartz.dart';
import 'package:ipwhois/common/failures.dart';
import 'package:ipwhois/domain/entities/ipinfo.dart';

abstract class IpInfoRepository {
  Future<Either<Failure, IpInfo>> getInfoByHostname(String ip);

  Future<Either<Failure, IpInfo>> getInfoDetails();
}
