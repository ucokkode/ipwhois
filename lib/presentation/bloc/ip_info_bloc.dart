// ignore_for_file: constant_identifier_names

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:ipwhois/common/failures.dart';
import 'package:ipwhois/domain/entities/ipinfo.dart';
import 'package:ipwhois/domain/usecases/get_ip_info_details.dart';

part 'ip_info_event.dart';
part 'ip_info_state.dart';

const String SERVER_FAILURE_MESSAGE = "Problem Internet Connection";
const String INVALID_HOSTNAME_MESSAGE = "Invalid Hostname";
const String UNEXPECTED_MESSAGE = "An Unexpected Error";

class IpInfoBloc extends Bloc<IpInfoEvent, IpInfoState> {
  final GetIpInfoDetailsUseCase getIpInfoDetails;
  final List<IpInfo> prevState = [];

  String _failureRails(Failure failure) {
    if (failure is ServerFailure) {
      return SERVER_FAILURE_MESSAGE;
    }
    if (failure is InvalidHostname) {
      return INVALID_HOSTNAME_MESSAGE;
    }
    return UNEXPECTED_MESSAGE;
  }

  IpInfoBloc({required this.getIpInfoDetails}) : super(Empty()) {
    on<GetInfoByHostname>((event, emit) async {
      if (event.hostname.isEmpty) {
        return;
      }
      emit(Loading());
      final either = await getIpInfoDetails(Hostname(hostname: event.hostname));
      either.fold(
        (failure) {
          emit(Error(error: _failureRails(failure)));
          if (prevState.isEmpty) {
            emit(Empty());
          } else {
            emit(Loaded(info: prevState.removeLast()));
          }
        },
        (info) {
          prevState.add(info);
          emit(Loaded(info: info));
        },
      );
    });

    on<GetInfo>((event, emit) async {
      emit(Loading());
      final either = await getIpInfoDetails(NoParams());
      either.fold(
        (failure) {
          emit(Error(error: _failureRails(failure)));
          if (prevState.isEmpty) {
            emit(Empty());
          } else {
            emit(Loaded(info: prevState.removeLast()));
          }
        },
        (info) {
          prevState.add(info);
          emit(Loaded(info: info));
        },
      );
    });
  }
}
