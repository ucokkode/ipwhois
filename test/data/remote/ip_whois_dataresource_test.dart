import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:ipwhois/common/constants.dart';
import 'package:ipwhois/data/remote/ipwhois.dto.dart';
import 'package:ipwhois/data/remote/ipwhois.resource.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../data_for_test/import_data.dart';
import 'ip_whois_dataresource_test.mocks.dart';

@GenerateMocks([http.Client])
void main() {
  late http.Client client;
  late IpWhoisDataResource resource;
  setUp(() {
    client = MockClient();
    resource = IpWhoisDataResourceImpl(client: client);
  });
  void _whenUnion({String? hostname, required bool exception}) {
    final url = hostname != null
        ? Uri.parse(Constants.baseUrl).replace(path: "json/$hostname")
        : Uri.parse(Constants.baseUrl);
    final response = hostname != null
        ? http.Response(includeStr("info_hostname.json"), 200)
        : http.Response(includeStr("info.json"), 200);

    when(client.get(
      url,
      headers: {
        'Content-Type': 'application/json',
      },
    )).thenAnswer((_) async {
      return exception ? http.Response("Something went Wrong", 404) : response;
    });
  }

  void setUpMockUpSuccess({String? hostname}) {
    _whenUnion(hostname: hostname, exception: false);
  }

  void setUpMockUpException({String? hostname}) {
    _whenUnion(hostname: hostname, exception: true);
  }

  group("getInfo", () {
    test("should return future of IpWhoisDTO", () async {
      setUpMockUpSuccess();

      final result = await resource.getInfoDetails();
      expect(result, isA<IpWhoisDTO>());
      expect(result.success, true);
      result.map(success: (actual) {
        expect(actual.ip, "194.63.249.234");
      }, error: (_) {
        fail("shouldn't return an IpWhoisError");
      });
    });
    test("should throw an HttpException", () async {
      setUpMockUpException();
      result() async => await resource.getInfoDetails();
      expect(result, throwsA(const TypeMatcher<HttpException>()));
    });
  });

  group("getInfoByHostname", () {
    const hostname = "google.com";
    test("should return future of IpWhoisDTO", () async {
      setUpMockUpSuccess(hostname: hostname);

      final result = await resource.getInfoByHostname(hostname);
      expect(result, isA<IpWhoisDTO>());
      expect(result.success, true);
      result.map(success: (actual) {
        expect(actual.ip, "74.125.24.102");
      }, error: (_) {
        fail("shouldn't return an IpWhoisError");
      });
    });
    test("should return future of IpWhoisError", () async {
      when(client.get(
        Uri.parse(Constants.baseUrl).replace(path: "json/hostname.fail"),
        headers: {
          'Content-Type': 'application/json',
        },
      )).thenAnswer((_) async {
        return http.Response(includeStr("fail.json"), 200);
      });
      final result = await resource.getInfoByHostname("hostname.fail");
      expect(result, isA<IpWhoisError>());
      expect(result.success, false);
      result.map(success: (actual) {
        fail("shouldn't return an IpWhoisDTO");
      }, error: (error) {
        expect(error.message, "invalid IP address");
      });
    });
    test("should throw an HttpException", () async {
      setUpMockUpException(hostname: hostname);
      result() async => await resource.getInfoByHostname(hostname);
      expect(result, throwsA(const TypeMatcher<HttpException>()));
    });
  });
}
