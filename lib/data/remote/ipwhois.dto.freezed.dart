// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'ipwhois.dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

IpWhoisUnion _$IpWhoisUnionFromJson(Map<String, dynamic> json) {
  switch (json['success']) {
    case 'success':
      return IpWhoisDTO.fromJson(json);
    case 'error':
      return IpWhoisError.fromJson(json);

    default:
      throw CheckedFromJsonException(json, 'success', 'IpWhoisUnion',
          'Invalid union type "${json['success']}"!');
  }
}

/// @nodoc
mixin _$IpWhoisUnion {
  bool get success => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String ip, bool success, String country,
            @JsonKey(name: "country_flag") String flag)
        success,
    required TResult Function(bool success, String message) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String ip, bool success, String country,
            @JsonKey(name: "country_flag") String flag)?
        success,
    TResult Function(bool success, String message)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String ip, bool success, String country,
            @JsonKey(name: "country_flag") String flag)?
        success,
    TResult Function(bool success, String message)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(IpWhoisDTO value) success,
    required TResult Function(IpWhoisError value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(IpWhoisDTO value)? success,
    TResult Function(IpWhoisError value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(IpWhoisDTO value)? success,
    TResult Function(IpWhoisError value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $IpWhoisUnionCopyWith<IpWhoisUnion> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $IpWhoisUnionCopyWith<$Res> {
  factory $IpWhoisUnionCopyWith(
          IpWhoisUnion value, $Res Function(IpWhoisUnion) then) =
      _$IpWhoisUnionCopyWithImpl<$Res>;
  $Res call({bool success});
}

/// @nodoc
class _$IpWhoisUnionCopyWithImpl<$Res> implements $IpWhoisUnionCopyWith<$Res> {
  _$IpWhoisUnionCopyWithImpl(this._value, this._then);

  final IpWhoisUnion _value;
  // ignore: unused_field
  final $Res Function(IpWhoisUnion) _then;

  @override
  $Res call({
    Object? success = freezed,
  }) {
    return _then(_value.copyWith(
      success: success == freezed
          ? _value.success
          : success // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$$IpWhoisDTOCopyWith<$Res>
    implements $IpWhoisUnionCopyWith<$Res> {
  factory _$$IpWhoisDTOCopyWith(
          _$IpWhoisDTO value, $Res Function(_$IpWhoisDTO) then) =
      __$$IpWhoisDTOCopyWithImpl<$Res>;
  @override
  $Res call(
      {String ip,
      bool success,
      String country,
      @JsonKey(name: "country_flag") String flag});
}

/// @nodoc
class __$$IpWhoisDTOCopyWithImpl<$Res> extends _$IpWhoisUnionCopyWithImpl<$Res>
    implements _$$IpWhoisDTOCopyWith<$Res> {
  __$$IpWhoisDTOCopyWithImpl(
      _$IpWhoisDTO _value, $Res Function(_$IpWhoisDTO) _then)
      : super(_value, (v) => _then(v as _$IpWhoisDTO));

  @override
  _$IpWhoisDTO get _value => super._value as _$IpWhoisDTO;

  @override
  $Res call({
    Object? ip = freezed,
    Object? success = freezed,
    Object? country = freezed,
    Object? flag = freezed,
  }) {
    return _then(_$IpWhoisDTO(
      ip: ip == freezed
          ? _value.ip
          : ip // ignore: cast_nullable_to_non_nullable
              as String,
      success: success == freezed
          ? _value.success
          : success // ignore: cast_nullable_to_non_nullable
              as bool,
      country: country == freezed
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String,
      flag: flag == freezed
          ? _value.flag
          : flag // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$IpWhoisDTO implements IpWhoisDTO {
  const _$IpWhoisDTO(
      {required this.ip,
      required this.success,
      required this.country,
      @JsonKey(name: "country_flag") required this.flag});

  factory _$IpWhoisDTO.fromJson(Map<String, dynamic> json) =>
      _$$IpWhoisDTOFromJson(json);

  @override
  final String ip;
  @override
  final bool success;
  @override
  final String country;
  @override
  @JsonKey(name: "country_flag")
  final String flag;

  @override
  String toString() {
    return 'IpWhoisUnion.success(ip: $ip, success: $success, country: $country, flag: $flag)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$IpWhoisDTO &&
            const DeepCollectionEquality().equals(other.ip, ip) &&
            const DeepCollectionEquality().equals(other.success, success) &&
            const DeepCollectionEquality().equals(other.country, country) &&
            const DeepCollectionEquality().equals(other.flag, flag));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(ip),
      const DeepCollectionEquality().hash(success),
      const DeepCollectionEquality().hash(country),
      const DeepCollectionEquality().hash(flag));

  @JsonKey(ignore: true)
  @override
  _$$IpWhoisDTOCopyWith<_$IpWhoisDTO> get copyWith =>
      __$$IpWhoisDTOCopyWithImpl<_$IpWhoisDTO>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String ip, bool success, String country,
            @JsonKey(name: "country_flag") String flag)
        success,
    required TResult Function(bool success, String message) error,
  }) {
    return success(ip, this.success, country, flag);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String ip, bool success, String country,
            @JsonKey(name: "country_flag") String flag)?
        success,
    TResult Function(bool success, String message)? error,
  }) {
    return success?.call(ip, this.success, country, flag);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String ip, bool success, String country,
            @JsonKey(name: "country_flag") String flag)?
        success,
    TResult Function(bool success, String message)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(ip, this.success, country, flag);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(IpWhoisDTO value) success,
    required TResult Function(IpWhoisError value) error,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(IpWhoisDTO value)? success,
    TResult Function(IpWhoisError value)? error,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(IpWhoisDTO value)? success,
    TResult Function(IpWhoisError value)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$IpWhoisDTOToJson(
      this,
    );
  }
}

abstract class IpWhoisDTO implements IpWhoisUnion {
  const factory IpWhoisDTO(
          {required final String ip,
          required final bool success,
          required final String country,
          @JsonKey(name: "country_flag") required final String flag}) =
      _$IpWhoisDTO;

  factory IpWhoisDTO.fromJson(Map<String, dynamic> json) =
      _$IpWhoisDTO.fromJson;

  String get ip;
  @override
  bool get success;
  String get country;
  @JsonKey(name: "country_flag")
  String get flag;
  @override
  @JsonKey(ignore: true)
  _$$IpWhoisDTOCopyWith<_$IpWhoisDTO> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$IpWhoisErrorCopyWith<$Res>
    implements $IpWhoisUnionCopyWith<$Res> {
  factory _$$IpWhoisErrorCopyWith(
          _$IpWhoisError value, $Res Function(_$IpWhoisError) then) =
      __$$IpWhoisErrorCopyWithImpl<$Res>;
  @override
  $Res call({bool success, String message});
}

/// @nodoc
class __$$IpWhoisErrorCopyWithImpl<$Res>
    extends _$IpWhoisUnionCopyWithImpl<$Res>
    implements _$$IpWhoisErrorCopyWith<$Res> {
  __$$IpWhoisErrorCopyWithImpl(
      _$IpWhoisError _value, $Res Function(_$IpWhoisError) _then)
      : super(_value, (v) => _then(v as _$IpWhoisError));

  @override
  _$IpWhoisError get _value => super._value as _$IpWhoisError;

  @override
  $Res call({
    Object? success = freezed,
    Object? message = freezed,
  }) {
    return _then(_$IpWhoisError(
      success: success == freezed
          ? _value.success
          : success // ignore: cast_nullable_to_non_nullable
              as bool,
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$IpWhoisError implements IpWhoisError {
  const _$IpWhoisError({required this.success, required this.message});

  factory _$IpWhoisError.fromJson(Map<String, dynamic> json) =>
      _$$IpWhoisErrorFromJson(json);

  @override
  final bool success;
  @override
  final String message;

  @override
  String toString() {
    return 'IpWhoisUnion.error(success: $success, message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$IpWhoisError &&
            const DeepCollectionEquality().equals(other.success, success) &&
            const DeepCollectionEquality().equals(other.message, message));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(success),
      const DeepCollectionEquality().hash(message));

  @JsonKey(ignore: true)
  @override
  _$$IpWhoisErrorCopyWith<_$IpWhoisError> get copyWith =>
      __$$IpWhoisErrorCopyWithImpl<_$IpWhoisError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String ip, bool success, String country,
            @JsonKey(name: "country_flag") String flag)
        success,
    required TResult Function(bool success, String message) error,
  }) {
    return error(this.success, message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String ip, bool success, String country,
            @JsonKey(name: "country_flag") String flag)?
        success,
    TResult Function(bool success, String message)? error,
  }) {
    return error?.call(this.success, message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String ip, bool success, String country,
            @JsonKey(name: "country_flag") String flag)?
        success,
    TResult Function(bool success, String message)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this.success, message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(IpWhoisDTO value) success,
    required TResult Function(IpWhoisError value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(IpWhoisDTO value)? success,
    TResult Function(IpWhoisError value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(IpWhoisDTO value)? success,
    TResult Function(IpWhoisError value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$IpWhoisErrorToJson(
      this,
    );
  }
}

abstract class IpWhoisError implements IpWhoisUnion {
  const factory IpWhoisError(
      {required final bool success,
      required final String message}) = _$IpWhoisError;

  factory IpWhoisError.fromJson(Map<String, dynamic> json) =
      _$IpWhoisError.fromJson;

  @override
  bool get success;
  String get message;
  @override
  @JsonKey(ignore: true)
  _$$IpWhoisErrorCopyWith<_$IpWhoisError> get copyWith =>
      throw _privateConstructorUsedError;
}
