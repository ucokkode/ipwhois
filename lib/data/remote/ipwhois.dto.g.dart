// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ipwhois.dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$IpWhoisDTO _$$IpWhoisDTOFromJson(Map<String, dynamic> json) => _$IpWhoisDTO(
      ip: json['ip'] as String,
      success: json['success'] as bool,
      country: json['country'] as String,
      flag: json['country_flag'] as String,
    );

Map<String, dynamic> _$$IpWhoisDTOToJson(_$IpWhoisDTO instance) =>
    <String, dynamic>{
      'ip': instance.ip,
      'success': instance.success,
      'country': instance.country,
      'country_flag': instance.flag,
    };

_$IpWhoisError _$$IpWhoisErrorFromJson(Map<String, dynamic> json) =>
    _$IpWhoisError(
      success: json['success'] as bool,
      message: json['message'] as String,
    );

Map<String, dynamic> _$$IpWhoisErrorToJson(_$IpWhoisError instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
    };
