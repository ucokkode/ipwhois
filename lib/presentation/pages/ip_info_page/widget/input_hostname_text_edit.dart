import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ipwhois/presentation/bloc/ip_info_bloc.dart';

class InputHostnameTextEdit extends StatelessWidget {
  final _controller = TextEditingController();

  InputHostnameTextEdit({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 5.0),
      child: TextFormField(
        controller: _controller,
        style: const TextStyle(color: Colors.white),
        onFieldSubmitted: (val) {
          _controller.clear();
          context.read<IpInfoBloc>().add(GetInfoByHostname(val));
        },
        decoration: const InputDecoration(
          labelText: "Hostname",
          fillColor: Colors.white24,
          filled: true,
        ),
      ),
    );
  }
}
