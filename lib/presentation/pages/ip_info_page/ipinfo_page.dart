import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ipwhois/di/get_it_init.dart';
import 'package:ipwhois/presentation/bloc/ip_info_bloc.dart';
import 'package:ipwhois/presentation/pages/ip_info_page/widget/info_section.dart';
import 'package:ipwhois/presentation/pages/ip_info_page/widget/input_hostname_text_edit.dart';
import 'package:ipwhois/presentation/pages/ip_info_page/widget/loading.dart';

class IpInfoPage extends StatelessWidget {
  const IpInfoPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => di<IpInfoBloc>(),
      child: Scaffold(
        backgroundColor: Colors.black,
        resizeToAvoidBottomInset: false,
        body: SafeArea(
          child: Column(
            children: <Widget>[
              InputHostnameTextEdit(),
              Expanded(
                child: BlocConsumer<IpInfoBloc, IpInfoState>(
                  listener: (context, state) {
                    if (state is Error) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text(state.error)),
                      );
                    }
                  },
                  buildWhen: (context, state) {
                    return state is! Error;
                  },
                  builder: (context, state) {
                    if (state is Empty) {
                      context.read<IpInfoBloc>().add(GetInfo());
                    }
                    if (state is Loading) {
                      return const LoadingSection();
                    }
                    if (state is Loaded) {
                      return InfoSection(info: state.info);
                    }
                    return Container(color: Colors.black);
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
