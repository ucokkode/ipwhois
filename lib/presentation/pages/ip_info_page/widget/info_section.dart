import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ipwhois/domain/entities/ipinfo.dart';

class InfoSection extends StatelessWidget {
  final IpInfo info;

  const InfoSection({super.key, required this.info});

  @override
  Widget build(BuildContext context) {
    const style = TextStyle(color: Colors.white);
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SvgPicture.network(
          info.flag,
          height: 50.0,
          width: 60.0,
          fit: BoxFit.fill,
        ),
        const SizedBox(width: 15.0),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              info.country,
              style:
                  style.copyWith(fontWeight: FontWeight.bold, fontSize: 32.0),
            ),
            Text(
              info.ip,
              style: style.copyWith(fontSize: 18.0),
            )
          ],
        ),
      ],
    );
  }
}
