import 'package:equatable/equatable.dart';

class IpInfo extends Equatable {
  final String ip;
  final String country;
  final String flag;

  const IpInfo({required this.ip, required this.country, required this.flag});

  @override
  // TODO: implement props
  List<Object> get props => [ip, country, flag];
}
