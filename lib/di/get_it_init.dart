import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:ipwhois/data/remote/ipwhois.resource.dart';
import 'package:ipwhois/data/repositories/ipinfo_repository_impl.dart';
import 'package:ipwhois/domain/repositories/ipinfo_repository.dart';
import 'package:ipwhois/domain/usecases/get_ip_info_details.dart';
import 'package:ipwhois/presentation/bloc/ip_info_bloc.dart';

final di = GetIt.I;

Future<void> init() async {
  di.registerLazySingleton<http.Client>(() => http.Client());

  // data_resource
  di.registerLazySingleton<IpWhoisDataResource>(
      () => IpWhoisDataResourceImpl(client: di()));

  // repositories
  di.registerLazySingleton<IpInfoRepository>(
      () => IpInfoRepositoryImpl(resource: di()));

  // use_cases
  di.registerLazySingleton<GetIpInfoDetailsUseCase>(
      () => GetIpInfoDetailsUseCase(repository: di()));

  // bloc
  di.registerFactory(() => IpInfoBloc(getIpInfoDetails: di()));
}
