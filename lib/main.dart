import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ipwhois/di/get_it_init.dart' as di;
import 'package:ipwhois/presentation/pages/ip_info_page/ipinfo_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.white10, // status bar color
    ));
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        useMaterial3: true,
        primarySwatch: Colors.amber,
      ),
      home: const IpInfoPage(),
    );
  }
}
