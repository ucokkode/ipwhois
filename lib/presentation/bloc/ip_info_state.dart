part of 'ip_info_bloc.dart';

abstract class IpInfoState extends Equatable {
  const IpInfoState();

  @override
  List<Object> get props => [];
}

class Empty extends IpInfoState {}

class Loading extends IpInfoState {}

class Loaded extends IpInfoState {
  final IpInfo info;

  const Loaded({required this.info});

  @override
  List<Object> get props => [info];
}

class Error extends IpInfoState {
  final String error;

  const Error({required this.error});

  @override
  List<Object> get props => [error];
}
