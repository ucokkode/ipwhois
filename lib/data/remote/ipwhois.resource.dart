import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:ipwhois/common/constants.dart';
import "package:ipwhois/data/remote/ipwhois.dto.dart";

abstract class IpWhoisDataResource {
  Future<IpWhoisUnion> getInfoByHostname(String hostname);

  Future<IpWhoisUnion> getInfoDetails();
}

class IpWhoisDataResourceImpl implements IpWhoisDataResource {
  final Uri url = Uri.parse(Constants.baseUrl);
  final Client client;

  IpWhoisDataResourceImpl({required this.client});

  Future<IpWhoisUnion> _getInfoFromUrl(Uri uri) async {
    final response = await client.get(uri, headers: {
      'Content-Type': 'application/json',
    });
    if (response.statusCode == 200) {
      return IpWhoisUnion.fromJson(json.decode(response.body));
    } else {
      throw const HttpException("fail to load");
    }
  }

  @override
  Future<IpWhoisUnion> getInfoByHostname(String hostname) async {
    final uriWithHostname = url.replace(path: "json/$hostname");
    return await _getInfoFromUrl(uriWithHostname);
  }

  @override
  Future<IpWhoisUnion> getInfoDetails() async {
    return await _getInfoFromUrl(url);
  }
}
