import 'dart:convert';
import 'dart:io';

import 'package:ipwhois/data/remote/ipwhois.dto.dart';
import 'package:ipwhois/domain/entities/ipinfo.dart';

const mockIpInfo = IpInfo(
  ip: "194.63.249.234",
  country: "Norway",
  flag: "https://cdn.ipwhois.io/flags/no.svg",
);
const mockIpInfoByHostname = IpInfo(
  ip: "74.125.24.102",
  country: "United States",
  flag: "https://cdn.ipwhois.io/flags/us.svg",
);

const mockInfoByHostname = IpWhoisUnion.success(
  ip: "74.125.24.102",
  success: true,
  country: "United States",
  flag: "https://cdn.ipwhois.io/flags/us.svg",
);

const mockInfo = IpWhoisDTO(
  ip: "194.63.249.234",
  success: true,
  country: "Norway",
  flag: "https://cdn.ipwhois.io/flags/no.svg",
);

const mockInfoFail = IpWhoisUnion.error(
  success: false,
  message: "invalid IP address",
);

Map<String, dynamic> includeJson(String filename) =>
    jsonDecode(includeStr(filename));

String includeStr(String filename) =>
    File("test/data_for_test/$filename").readAsStringSync();
