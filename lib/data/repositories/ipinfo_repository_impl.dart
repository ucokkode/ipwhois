import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:ipwhois/common/failures.dart';
import 'package:ipwhois/data/mappers/ipwhoisdto_to_ipinfo.dart';
import 'package:ipwhois/data/remote/ipwhois.dto.dart';
import 'package:ipwhois/data/remote/ipwhois.resource.dart';
import 'package:ipwhois/domain/entities/ipinfo.dart';
import 'package:ipwhois/domain/repositories/ipinfo_repository.dart';

class IpInfoRepositoryImpl implements IpInfoRepository {
  final IpWhoisDataResource resource;

  const IpInfoRepositoryImpl({required this.resource});

  Future<Either<Failure, IpInfo>> _getInfoRails(
      Future<IpWhoisUnion> Function() getFn) async {
    try {
      final info = await getFn();
      return info.map(
        success: (success) => Right(success.toIpInfo()),
        error: (_) => Left(InvalidHostname()),
      );
    } on HttpException {
      return Left(ServerFailure());
    } catch (_) {
      return Left(UnexpectedFailure());
    }
  }

  @override
  Future<Either<Failure, IpInfo>> getInfoByHostname(String ip) async {
    return await _getInfoRails(() => resource.getInfoByHostname(ip));
  }

  @override
  Future<Either<Failure, IpInfo>> getInfoDetails() async {
    return await _getInfoRails(() => resource.getInfoDetails());
  }
}
