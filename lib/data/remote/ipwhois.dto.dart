import 'package:freezed_annotation/freezed_annotation.dart';

part "ipwhois.dto.freezed.dart";
part "ipwhois.dto.g.dart";

@Freezed(unionKey: "success")
class IpWhoisUnion with _$IpWhoisUnion {
  const factory IpWhoisUnion.success({
    required final String ip,
    required final bool success,
    required final String country,
    @JsonKey(name: "country_flag")
    required final String flag,
  }) = IpWhoisDTO;

  const factory IpWhoisUnion.error({
    required final bool success,
    required final String message,
  }) = IpWhoisError;

  factory IpWhoisUnion.fromJson(Map<String, dynamic> json) =>
      IpWhoisConverter().fromJson(json);
}

class IpWhoisConverter
    implements JsonConverter<IpWhoisUnion, Map<String, dynamic>> {
  @override
  IpWhoisUnion fromJson(Map<String, dynamic> json) {
    if (json['success']) {
      return IpWhoisDTO.fromJson(json);
    } else {
      return IpWhoisError.fromJson(json);
    }
  }

  @override
  Map<String, dynamic> toJson(IpWhoisUnion data) => data.toJson();
}
