import 'package:flutter_test/flutter_test.dart';
import 'package:ipwhois/data/mappers/ipwhoisdto_to_ipinfo.dart';

import '../../data_for_test/import_data.dart';

void main() {
  test("should return IpInfo entity from IpWhoisDTO", () {
    final data = mockInfo.toIpInfo();
    expect(data, mockIpInfo);
  });
}
