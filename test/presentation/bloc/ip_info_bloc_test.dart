import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:ipwhois/common/failures.dart';
import 'package:ipwhois/domain/usecases/get_ip_info_details.dart';
import 'package:ipwhois/presentation/bloc/ip_info_bloc.dart' as bloclib;
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../data_for_test/import_data.dart';
import 'ip_info_bloc_test.mocks.dart';

@GenerateMocks([GetIpInfoDetailsUseCase])
void main() {
  late GetIpInfoDetailsUseCase getIpInfoDetailsUseCase;
  late bloclib.IpInfoBloc bloc;
  setUp(() {
    getIpInfoDetailsUseCase = MockGetIpInfoDetailsUseCase();
    bloc = bloclib.IpInfoBloc(getIpInfoDetails: getIpInfoDetailsUseCase);
  });

  test("initial should empty", () {
    expect(bloc.state, bloclib.Empty());
  });

  group("on GetInfoByHostname", () {
    const hostname = "google.com";
    test("should invoke usecase", () async {
      when(getIpInfoDetailsUseCase(const Hostname(hostname: hostname)))
          .thenAnswer((_) async => const Right(mockIpInfoByHostname));
      bloc.add(const bloclib.GetInfoByHostname(hostname));
      await untilCalled(
          getIpInfoDetailsUseCase(const Hostname(hostname: hostname)));
      verify(getIpInfoDetailsUseCase(const Hostname(hostname: hostname)));
    });
    test("should emit [ Loading Loaded ] ", () async {
      when(getIpInfoDetailsUseCase(const Hostname(hostname: hostname)))
          .thenAnswer((_) async => const Right(mockIpInfoByHostname));
      final expected = <bloclib.IpInfoState>[
        bloclib.Loading(),
        const bloclib.Loaded(info: mockIpInfoByHostname)
      ];
      expect(bloc.state, bloclib.Empty());
      bloc.add(const bloclib.GetInfoByHostname(hostname));
      await expectLater(bloc.stream, emitsInOrder(expected));
      expect(bloc.prevState.last, mockIpInfoByHostname);
    });
    test(
        "should emit [ Loading Error Empty ], initial state [ Empty ] and back to [ Empty ] if prevState doesn't initialized",
        () async {
      when(getIpInfoDetailsUseCase(const Hostname(hostname: "hostname.fail")))
          .thenAnswer((_) async => Left(InvalidHostname()));
      final expected = <bloclib.IpInfoState>[
        bloclib.Loading(),
        const bloclib.Error(error: bloclib.INVALID_HOSTNAME_MESSAGE),
        bloclib.Empty()
      ];
      expect(bloc.state, bloclib.Empty());
      bloc.add(const bloclib.GetInfoByHostname("hostname.fail"));
      await expectLater(bloc.stream, emitsInOrder(expected));
      expect(bloc.prevState.isEmpty, true);
    });
  });
  group("on GetInfo", () {
    test("should invoke usecase", () async {
      when(getIpInfoDetailsUseCase(NoParams()))
          .thenAnswer((_) async => const Right(mockIpInfo));
      bloc.add(bloclib.GetInfo());
      await untilCalled(getIpInfoDetailsUseCase(NoParams()));
      verify(getIpInfoDetailsUseCase(NoParams()));
    });
    test("should emit [ Loading Loaded ] ", () async {
      when(getIpInfoDetailsUseCase(NoParams()))
          .thenAnswer((_) async => const Right(mockIpInfo));
      final expected = <bloclib.IpInfoState>[
        bloclib.Loading(),
        const bloclib.Loaded(info: mockIpInfo)
      ];
      expect(bloc.state, bloclib.Empty());
      bloc.add(bloclib.GetInfo());
      await expectLater(bloc.stream, emitsInOrder(expected));
      expect(bloc.prevState.last, mockIpInfo);
    });
    test(
        "should emit [ Loading Error Empty ], initial state [ Empty ] and back to [ Empty ] if prevState doesn't initialized",
        () async {
      when(getIpInfoDetailsUseCase(NoParams()))
          .thenAnswer((_) async => Left(ServerFailure()));
      final expected = <bloclib.IpInfoState>[
        bloclib.Loading(),
        const bloclib.Error(error: bloclib.SERVER_FAILURE_MESSAGE),
        bloclib.Empty()
      ];
      expect(bloc.state, bloclib.Empty());
      bloc.add(bloclib.GetInfo());
      await expectLater(bloc.stream, emitsInOrder(expected));
      expect(bloc.prevState.isEmpty, true);
    });
  });
}
