import 'package:dartz/dartz.dart';
import 'package:ipwhois/common/failures.dart';

abstract class UserCases<Type, Params> {
  Future<Either<Failure, Type>> call(Params param);
}
